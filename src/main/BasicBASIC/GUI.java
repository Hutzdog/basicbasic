package main.BasicBASIC;

import javax.swing.JFrame;
import java.awt.FlowLayout; 
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

final class GUI extends JFrame {
  private static final long serialVersionUID = 1L;

  JFrame f = new JFrame();
  JTextArea out;
  JScrollPane pane;
  
  void log(final String text) {
    out.append(text + '\n');
  }

  GUI () {
    f.getContentPane().setLayout(new FlowLayout());
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    out = new JTextArea(32,64);
    pane = new JScrollPane(out);

    pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);  

    f.getContentPane().add(pane);

    f.pack();
    f.setVisible(true);
  }
}