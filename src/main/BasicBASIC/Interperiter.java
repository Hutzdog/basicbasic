package main.BasicBASIC;

import java.util.List;

import main.BasicBASIC.Parser.Instruction;

public class Interperiter {
  public int exitCode = 0;

  public Interperiter(List<Instruction> instructions) {
    interperit(instructions, 0);
  }

  private void interperit(List<Instruction> instructions, int startIndex) {
    boolean skipAdd = false;
    mainloop: for (int pos = startIndex; pos <= instructions.size(); pos += (skipAdd) ? 0 : 1) {
      if(Util.errored) return;

      skipAdd = false;
      Instruction i = instructions.get(pos);
      switch (i.type) {
        case PRINT:
          Util.log(i.args.get(0).value);
          break;

        case GOTO:
          int value = Integer.parseInt(i.args.get(0).value);
          boolean err = true;
          int newIndex = pos + 1;

          for(Instruction jmpto : instructions.subList(pos, instructions.size())) {
            if (jmpto.number == value) {
              pos = newIndex;
              skipAdd = true;
              continue mainloop;
            }
            newIndex++;
          }
          
          newIndex = 0;
          for(Instruction jmpto : instructions.subList(0, pos)) {
            if(jmpto.number == value) {
              pos = newIndex;
              skipAdd = true;
              continue mainloop;
            }
            newIndex++;
          }

          if(err) Util.handleError(31, "Invalid GOTO index");
          break;
        case GOTOR:
          int valuep = Integer.parseInt(i.args.get(0).value);
          int newIndexp = pos;
          boolean errp = true;

          for(Instruction jmpto : instructions.subList(pos, instructions.size())) {
            if(jmpto.number == valuep) {
              interperit(instructions, newIndexp);
              errp = false;
            }
            newIndexp++;
          }

          newIndexp = 0;
          for(Instruction jmpto : instructions.subList(0, pos)) {
            if(jmpto.number == valuep) {
              interperit(instructions, newIndexp);
              errp = false;
            }
            newIndexp++;
          }

          if(errp) Util.handleError(31, "Invalid GOTO index");
          break;
        
        case HALT:
          break mainloop;

        default:
          break;
      }
    }
  }
}