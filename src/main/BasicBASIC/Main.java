package main.BasicBASIC;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Main {
  public static final String version = "0.1.0";

  public static void main(String[] args) {
    //Initialize GUI
    Util.gui = new GUI();
    Util.log("BasicBASIC V" + version);

    if(args.length != 1) {
      Util.handleError(1, "Invalid Usage. USAGE: basicbasic <filename>");
    }

    String input = "";
    try {
      input = readInput(args[0]);
    }
    catch(IOException e) {
      Util.handleError(5, "Could not read file");
    }

    if(!Util.errored) {
      Lexer lexer = new Lexer(input);
      Util.debug(lexer.tokens.toString());
      if(!Util.errored) {
        Parser parser = new Parser(lexer.tokens);
        Util.debug(parser.instructions + "\n");

        if(!Util.errored) {
          Interperiter interperiter = new Interperiter(parser.instructions);
          Util.exit(interperiter.exitCode);
        }
      }
    }
  }

  private static String readInput(String filename) throws IOException {
    return new String(Files.readAllBytes(Paths.get(filename)));
  }
}