package main.BasicBASIC;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.BasicBASIC.Lexer.Token;
import main.BasicBASIC.Lexer.Token.TokenType;

public class Parser {
  public static class Instruction {
    public final int number;
    public final TokenType type;
    public final List<Token> args;

    private Instruction(int number, TokenType type, List<Token> args) {
      this.number = number;
      this.type = type;
      this.args = args;
    }

    public String toString() {
      return "[" + number + " " + type + " " + args + "]";
    }
  }
  public final List<Instruction> instructions;

  public Parser(List<Token> toks) {
    int mode = 0; //Modes: 0=number 1=type 2=args
    int curNumber = 0; 
    TokenType curType = null; 
    List<Token> curArgs = new ArrayList<>();
    instructions = new ArrayList<>();

    for (Token tok : toks) {
      if(mode == 0) {
        if(tok.type != TokenType.NUMBER) Util.handleError(21, "Invalid line number. Line numbers are handled from the end of the program to the beginning and must be numeric.");
        curNumber = Integer.parseInt(tok.value);
        mode++;
      }
      else if(mode == 1) {
        TokenType[] valid = {TokenType.GOTO, TokenType.GOTOR, TokenType.PRINT, TokenType.HALT, TokenType.COMMENT};
        if(Arrays.asList(valid).contains(tok.type)) {
          curType = tok.type;
          mode++;
        }

        else Util.handleError(22, "Invalid Instruction: " + curType);
      }
      else if(mode == 2) {
        TokenType[] valid = {TokenType.NUMBER, TokenType.STRING};
        if(tok.type == TokenType.ENDL) {
          instructions.add(new Instruction(curNumber, curType, curArgs));
          curNumber = 0; curType = null; curArgs = new ArrayList<>();
          mode = 0;
        }
        else if(Arrays.asList(valid).contains(tok.type)) curArgs.add(tok);

        else Util.handleError(23, "Invalid Argument: " + tok.type);
      }
    }

    if(mode != 0) Util.handleError(24, "Unexpected EOF during instruction");
  }
}