package main.BasicBASIC;

public class Util {
  static GUI gui;
  static boolean errored = false;
  private Util() {}

  public static void handleError(final int errorCode, final String message) {
    log("Error > " + message);
    errored = true;
    exit(errorCode);
  }

  public static void log(final String message) {
    gui.log(message);
  }

  public static void warn(final String message) {
    log("Warn > " + message);
  }

  //Debug information, intentionally in console
  public static void debug(final String message) {
    System.out.println("DEBUG > " + message);
  }

  public static void exit(final int code) {
    log("Program exited with code " + code + ".");
  }
}