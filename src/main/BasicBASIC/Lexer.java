package main.BasicBASIC;

import java.util.ArrayList;
import java.util.List;

import main.BasicBASIC.Lexer.Token.TokenType;

public final class Lexer {
  public final List<Token> tokens;

  public static class Token {
    public final String value;
    public final TokenType type;

    enum TokenType {
      NUMBER, STRING, PRINT, GOTO, GOTOR, ENDL, HALT, COMMENT,
    }

    public Token(String value, TokenType type) {
      this.value = value;
      this.type = type;
    }

    @Override
    public String toString() {
      return type + " > " + value;
    }
  }

  public Lexer(String input) {
    List<Token> out = new ArrayList<>();
    String buf = "";
    boolean isComment = false;
    boolean strMode = false;
    boolean extStrMode = false;

    for(char c : input.toCharArray()) {
      if(isComment) {
        if(c == '\n') {
          isComment = false;
        }
      }
      else if(strMode) {
        if(c == '"') {
          out.add(new Token(buf, TokenType.STRING));
          buf = "";
          strMode = false;
          extStrMode = true;
          continue;
        }
        buf += c;
      }
      else if(c != '\n' && c != '\r') {
        if(c == ' ' || c == '\t') {
          extStrMode = false;
          out.add(matchBuffer(buf));
          buf = "";
        }
        else {
          switch(c) {
            case ';':
              if(!extStrMode)
                out.add(matchBuffer(buf));
              
              out.add(new Token(";", TokenType.ENDL));
              buf = "";
              isComment = true;
              break;
            case '"':
              strMode = true;
              break;
            default:
              buf += c;
              break;
          }
        }
      }
    }

    tokens = out;
  }

  private static boolean isNumeric(String input) {
    final char[] matches = {'0','1','2','3','4','5','6','7','8','9'};
    mainloop: for(char c : input.toCharArray()) {
      for(char match : matches) {
        if(c == match) continue mainloop;
      }
      return false;
    }
    return true;
  }

  private static Token matchBuffer(String buf) {
    switch(buf) {
      case "PRINT":
        return new Token("print", TokenType.PRINT);

      case "GOTO":
        return new Token("goto", TokenType.GOTO);
      
       case "GOTOR":
        return new Token("gotor", TokenType.GOTOR);
      
       case "HALT":
        return new Token("halt", TokenType.HALT);

       case "COMMENT":
        return new Token("comment", TokenType.COMMENT);

       default:
        if(isNumeric(buf)) {
          return new Token(buf, TokenType.NUMBER);
        }

        Util.handleError(11, "Invalid Token: " + buf);
        return new Token(null, null);
    }
  }
}