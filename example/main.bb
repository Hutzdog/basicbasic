0 PRINT "This is BasicBasic, a programming language in under 750 SLOC(significant lines of code) of Java. Here, we will write a simple 'function' and 'loop'";
0 GOTOR 1001; This is a comment. The previous expression jumps to a line that uses the sorting algorithm listed in the wiki to jump to another line. This expression starts a new parser and hence will resume where it left off once exited.
1 PRINT "This loop is infinite"; Here we will demonstrate loops and GOTR's functionality
0 GOTO 1; This is a simple loop
0 HALT; This won't be run, but it's here anyways and is good practice to have

1001 PRINT "This is now in a function";
0 PRINT "It is not a large function";
0 HALT; This demonstates HALTing. It tells the current parsr to stop running.
0 PRINT "This will never run";