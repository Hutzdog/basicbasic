0 PRINT "Example program, currently in main execution area";
0 GOTOR 1001;
0 HALT; After here, we see an example function

1001 PRINT "An example of a 'function'";
0 PRINT "Stylistically, it is best to have all function jumps be categorized into 3-digit number 'namespaces', here this would be namespace 1000.";
0 PRINT "It is also best to have all lines you don't jump to use the number 0, allowing for less ambiguity when working with GOTOs";
0 HALT;

0 PRINT "Here, we see an example of an infinite loop";
1 PRINT "test";
0 GOTO 1; This GOTO differs from GOTOR in that it re-uses the parser, allowing for loops that don't stack overflow